package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Combo;
import models.Shop;
import models.products.Product;
import models.products.ProductType;
import models.products.StockeableProduct;
import play.api.Mode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ShopController  extends Controller {

    private static List<Shop> shops = new ArrayList<Shop>();

    //Request:

    public Result addShop(){
        JsonNode json = request().body().asJson();
        Shop shop = Json.fromJson(json, Shop.class);
        shops.add(shop);
        return ok(Json.toJson(shops));
    }

    public Result getStockFromShop(long shopId){
        int shopID = (int) shopId;
        Optional<Shop> shopOptional = getShopFromId(shopID);
        if(!shopOptional.isPresent()) return badRequest("Shop Not Found");

        return ok(Json.toJson(shopOptional.get().getStockeableStock()));
    }

    //Not Request:

    public int getFullStock(Product product){
        int fullStock = 0;
        for (Shop shop: shops){
            fullStock += shop.getStock(product);
        }
        return fullStock;
    }

    public Optional<Shop> getShopFromId(int id){
        for (Shop shop : shops) {
            if (shop.getId() == id) return Optional.of(shop);
        } return Optional.empty();
    }

    public boolean sell(Product product, int shopID){
        Optional<Shop> shopOptional = getShopFromId(shopID);
        if (!shopOptional.isPresent()) return false;
        Shop shop = shopOptional.get();

        if(product.getProductType() == ProductType.VIRTUAL) return false;
        StockeableProduct stockable = ((StockeableProduct) product);
        if (shop.getStock(stockable) > 0) shop.sell(product);
        else return false;

        return true;
    }

    public boolean sell(Product product, int shopID, int quantity){
        Optional<Shop> shopOptional = getShopFromId(shopID);
        if (!shopOptional.isPresent()) return false;
        Shop shop = shopOptional.get();

        if(product.getProductType() == ProductType.VIRTUAL) return false;
        StockeableProduct stockable = ((StockeableProduct) product);
        if (shop.getStock(stockable) > quantity) shop.sell(product, quantity);
        else return false;

        return true;
    }

    public Optional<Shop> getShopWithStock(Product product){
        for (Shop shop : shops) {
            if (shop.getStock(product) > 0) return Optional.of(shop);
        } return Optional.empty();
    }

    public Optional<Shop> getShopWithStock(Combo combo){
        for (Shop shop: shops) {
            if (hasAllProducts(shop, combo)) return Optional.of(shop);
        } return Optional.empty();
    }

    private boolean hasAllProducts(Shop shop, Combo combo){
        for (Product product : combo.getProducts().keySet()) {
            if (shop.getStock(product) < combo.getProducts().get(product)) return false;
        }
        return true;
    }

}
