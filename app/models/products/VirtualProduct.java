package models.products;

import java.util.List;

public class VirtualProduct extends Product {


    public VirtualProduct(){}

    @Override
    public ProductType getProductType() {
        return ProductType.VIRTUAL;
    }

    public VirtualProduct(String name, String description, float actualPrice, List<Category> categories) {
        super(name, description, actualPrice, categories);
    }
}
