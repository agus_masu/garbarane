package models;

import models.Interfaces.Sellable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Sale extends BaseModel {

    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
    private User user;
    private List<Sellable> sellables;
    private Date date;


    public Sale() {
        super(ID_GENERATOR.getAndIncrement());
        date = new Date();
        sellables = new ArrayList<>();
    }

    public Sale(User user){
        super(ID_GENERATOR.getAndIncrement());
        this.user = user;
        date = new Date();
        sellables = new ArrayList<>();
    }

    public void addSellable(Sellable sellable){
        sellables.add(sellable);
    }

    public User getUser() {
        return user;
    }

    public List<Sellable> getSellables() {
        return sellables;
    }

    public Date getDate() {
        return date;
    }
}
