package models;

public class BaseModel {
    private int id;

    public int getId() {
        return id;
    }

    public BaseModel(int id) {
        this.id = id;
    }
}
