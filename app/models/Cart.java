package models;

import models.Interfaces.Sellable;

import java.util.HashMap;
import java.util.Map;

public class Cart {
    public Map<Sellable, Integer> sellables;
    public Cart(){
        sellables = new HashMap<>();
    }

    public Cart(Map<Sellable, Integer> sellables){
        this.sellables = sellables;
    }

    public void addSellable(Sellable sellable, int quantity)    {
        if (!sellables.containsKey(sellable)) sellables.put(sellable, quantity);
        else{
            final int value = sellables.get(sellable) + quantity;
            sellables.put(sellable, value);
        }
    }
}
