package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ser.Serializers;
import models.Interfaces.Sellable;
import models.products.Category;
import models.products.Product;
import models.products.SellableType;
import play.api.Mode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Combo extends BaseModel implements Sellable {

    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
    private Map<Product, Integer> products;
    private String name;
    private float price;

    public Combo() {
        super(ID_GENERATOR.getAndIncrement());
        products = new HashMap<>();
    }

    public Combo(Map<Product, Integer> map, String name){
        super(ID_GENERATOR.getAndIncrement());
        this.products = map;
        this.name = name;
    }


    @Override
    public float getPrice() {
        float finalPrice = 0f;
        for (Product product : products.keySet()) {
            finalPrice += product.getPrice() * products.get(product);
        }return finalPrice;
    }

    @Override
    public SellableType getType() {
        return SellableType.COMBO;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @JsonIgnore
    public List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();
        for (Product product : products.keySet()) {
            for (Category category : product.getCategories()) {
                if (!categories.contains(category)) categories.add(category);
            }
        } return categories;
    }

    @JsonIgnore
    public Map<Product, Integer> getProducts() {
        return products;
    }
}
