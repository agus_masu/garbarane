package models.Interfaces;

import models.payments.CardInfo;

public interface PaymentMethod {

    public boolean verify(float mount, CardInfo info);

    public void charge(float mount, CardInfo info);
}
