package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import models.Combo;
import models.Shop;
import models.input.InputCombo;
import models.products.Product;
import models.products.ProductType;
import models.products.StockeableProduct;
import models.products.VirtualProduct;
import play.api.Mode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.*;

public class StockController extends Controller {

    @Inject
    private ShopController shopController;

    private static List<Product> availableProducts = new ArrayList<>();
    private static List<Combo> combos = new ArrayList<>();


    //Request methods:

    public Result addVirtual(){
        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("JsonExpected");
        VirtualProduct product = Json.fromJson(json, VirtualProduct.class);
        availableProducts.add(product);
        return ok(Json.toJson(product));
    }

    public Result addStockable(){
        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("JsonExpected");
        StockeableProduct product = Json.fromJson(json, StockeableProduct.class);
        availableProducts.add(product);
        return ok(Json.toJson(product));
    }

    public Result addProductToShop(){
        final JsonNode json = request().body().asJson();
        if (json == null) return badRequest("JSON Expected");
        final int productId = json.findPath("productId").asInt();
        final int shopId = json.findPath("shopId").asInt();
        final int reStocking = json.findPath("stock").asInt();

        if (reStocking <= 0 ) return badRequest("Bad restocking number");

        Optional<Shop> shopOptional = shopController.getShopFromId(shopId);
        if (!shopOptional.isPresent()) return badRequest("Shop Not Found");
        Shop shop = shopOptional.get();

        Optional<Product> productOptional = getProductFromID(productId);
        if (!productOptional.isPresent()) return badRequest("Product Not Found");
        Product product = productOptional.get();
        if (product.getProductType() == ProductType.VIRTUAL) return badRequest("That is a Virtual Product");

        StockeableProduct stockeable = ((StockeableProduct) product);

        shop.addToStock(stockeable, reStocking);

        return ok(Json.toJson(shop));
    }

    public Result getProducts(){
        if (availableProducts == null) return notFound();
        return ok(Json.toJson(availableProducts));
    }

    public Result getProductStockFromID(long productID){
        int id = (int) productID;
        final Optional<Product> productOptional = getProductFromID(id);
        if (!productOptional.isPresent()) return notFound("Produc3t Not Found");
        final int fullStock = shopController.getFullStock(productOptional.get());
        return ok(Json.toJson(fullStock));
    }

    public Result getShopWithComboStock(){
        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("BAD");
        int comboid = json.findPath("id").asInt();
        Optional<Combo> comboOptional = getComboFromId(comboid);
        if (!comboOptional.isPresent()) return badRequest("Bad Requesr");
        Combo combo = comboOptional.get();

        Optional<Shop>  shopOptional = getShopWithStock(combo);
        if(!shopOptional.isPresent()) return notFound("Theres not such Shop");

        return ok(Json.toJson(shopOptional.get()));

    }

    public Result addCombo(){
        JsonNode json = request().body().asJson();
        InputCombo input = Json.fromJson(json, InputCombo.class);

        Map<Product, Integer> map = new HashMap<>();
        for (Integer sellable : input.sellables) {
            Optional<Product> productOptional = getProductFromID(sellable);
            if (!productOptional.isPresent()) return notFound("Product "+ sellable + " Not Found");
            Product product = productOptional.get();
            if (!map.containsKey(product)) map.put(product, 1);
            else{
                final int value = map.get(product) + 1;
                map.put(product, value);
            }
        }

        Combo combo = new Combo(map, input.name);
        combos.add(combo);
        return ok(Json.toJson(combo));
    }

    public Result getAllCombos(){
        return ok(Json.toJson(combos));
    }



    //Not Request

    public Optional<Combo> getComboFromId(int comboId){
        for (Combo combo : combos) {
            if(combo.getId() == comboId) return Optional.of(combo);
        } return Optional.empty();
    }

    public Optional<Product> getProductFromID(int id){
        for (Product product: availableProducts){
            if (product.getId() == id) return Optional.of(product);
        }
        return Optional.empty();
    }

    public Optional<Integer> getFullStock(int id){
        Optional<Product> productOptional = getProductFromID(id);
        if (!productOptional.isPresent()) return Optional.empty();
        return Optional.of(shopController.getFullStock(productOptional.get()));
    }

    public boolean sell (int shopID, int productID){
         return shopController.sell(getProductFromID(productID).get(), shopID);
    }

    public boolean sell (int shopID, int productID, int quantity){
        return shopController.sell(getProductFromID(productID).get(), shopID);
    }

    public Optional<Shop> getShopWithStock(Combo combo){
        return shopController.getShopWithStock(combo);
    }
}
