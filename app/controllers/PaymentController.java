package controllers;

import models.Interfaces.PaymentMethod;
import models.payments.CardInfo;

public class PaymentController {

    public PaymentMethod paymemtMethod;

    public boolean verify(float mount, CardInfo info){
        return paymemtMethod.verify(mount, info);
    }

    public void charge(float mount, CardInfo info){
        paymemtMethod.charge(mount, info);
    }
}
