package models.payments;

import javax.smartcardio.Card;
import java.util.Date;

public class CardInfo {

    private long number;
    private Date expiration;
    private String holderName;
    private long securityCode;

    public CardInfo(long number, Date expiration, String holderName, long securityCode) {
        this.number = number;
        this.expiration = expiration;
        this.holderName = holderName;
        this.securityCode = securityCode;
    }

    public CardInfo(){}

    public long getNumber() {
        return number;
    }

    public Date getExpiration() {
        return expiration;
    }

    public String getHolderName() {
        return holderName;
    }

    public long getSecurityCode() {
        return securityCode;
    }
}
