package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import models.User;
import models.products.Product;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonController extends Controller {

    private static List<User> users = new ArrayList<>();

    @Inject
    private StockController stockController;

    //Requests:

    public Result addUser(){
        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("Invalid Request");
        User user = Json.fromJson(json, User.class);
        users.add(user);

        return ok(Json.toJson(user));
    }

    public Result addProductToCart(long userId){
        int userID = (int) userId;

        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("Bad Request");
        int productId = json.findPath("productId").asInt();

        Optional<Product> productOptional = stockController.getProductFromID(productId);
        if (!productOptional.isPresent()) return badRequest("Product Not Found");
        final Product product = productOptional.get();

        Optional<User> userOptional = getUserFromID(userID);
        if (!userOptional.isPresent()) return badRequest("User Not Found");
        User user = userOptional.get();

        int quantity = json.findPath("quantity").asInt();
        user.addSellablesToCart(product, quantity);

        return ok(Json.toJson(user.getCart()));


    }

    public Result getCart(long userId){
        int id = (int) userId;

        Optional<User> userOptional = getUserFromID(id);
        if(!userOptional.isPresent()) return badRequest("Bad Request, User Npt Found");

        User user = userOptional.get();
        return ok(Json.toJson(user.getCart()));
    }

    public Result delete(long id){
        int userId = (int) id;
        for (User user : users) {
            if(user.getId() == userId){
                users.remove(user);
                return ok(Json.toJson(userId));
            }
        } return notFound("User Not Found");
    }

    public Result get(long id){
        final int userId = (int) id;
        Optional<User> userOptional = getUserFromID(userId);
        if (!userOptional.isPresent()) return notFound("User not found");
        return ok(Json.toJson(userOptional.get()));
    }

    //Not Requests:

    public Optional<User> getUserFromID(int userId){
        for (User user : users) {
            if (user.getId() == userId) return Optional.of(user);
        }return Optional.empty();
    }
}
