package models;

public class Address {
    private String street;
    private String city;
    private String country;
    private String state;
    private int number;

    public Address(String street, String city, String country, String state, int number) {
        this.street = street;
        this.city = city;
        this.country = country;
        this.state = state;
        this.number = number;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }

    public int getNumber() {
        return number;
    }
}
