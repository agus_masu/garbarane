package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import models.Combo;
import models.Sale;
import models.Shop;
import models.User;
import models.products.Product;
import models.products.ProductType;
import play.api.Mode;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SellController extends Controller {

    private List<Sale> sales = new ArrayList<>();

    @Inject
    private PaymentController paymentController;

    @Inject
    private PersonController personController;

    @Inject
    private StockController stockController;

    //Requests:

    public Result sellStockableProduct(){
        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("Invalid Request");
        final int productID = json.findPath("productId").asInt();
        final int userID = json.findPath("userId").asInt();
        final int shopID = json.findPath("shopId").asInt();


        final Optional<User> userOptional = personController.getUserFromID(userID);
        if (!userOptional.isPresent()) return badRequest("User not found");
        User user = userOptional.get();

        final Optional<Product> productOptional = stockController.getProductFromID(productID);
        if(!productOptional.isPresent()) return badRequest("Product Not Found");
        Product product = productOptional.get();

        if(stockController.sell(shopID, productID) == false) return badRequest("Problem with Shops");

        Sale sale =  new Sale(user);
        sale.addSellable(product);
        sales.add(sale);

        return ok(Json.toJson(sale));
    }

    public Result sellVirtual(){
        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("JSON Problem");

        int productId = json.findPath("productId").asInt();
        int userId = json.findPath("userId").asInt();

        Optional<User> userOptional = personController.getUserFromID(userId);
        Optional<Product> productOptional = stockController.getProductFromID(productId);

        if (!userOptional.isPresent() || !productOptional.isPresent()) return badRequest("Error");

        User user = userOptional.get();
        Product product = productOptional.get();

        if (product.getProductType() == ProductType.STOCKEABLE) return badRequest("This is not virtual");

        Sale sale = new Sale(user);
        sale.addSellable(product);

        sales.add(sale);

        return ok(Json.toJson(sales));
    }

    public Result getAllSales(){
        return ok(Json.toJson(sales));
    }

    public Result getSalesByUser(long id){
        int userId = (int) id;
        List<Sale> userSales = new ArrayList<>();
        for (Sale sale : sales) {
            if (sale.getUser().getId() == userId) userSales.add(sale);
        }
        return ok(Json.toJson(userSales));
     }

     public Result sellCombo(){
        JsonNode json = request().body().asJson();
        if (json == null) return badRequest("JSON Problem");

        int comboId = json.findPath("comboId").asInt();

        Optional<Combo> comboOptional = stockController.getComboFromId(comboId);
        if (!comboOptional.isPresent()) return badRequest("Combo not found");

        Combo combo = comboOptional.get();

        Optional<Shop> shopOptional =  stockController.getShopWithStock(combo);
        if (!shopOptional.isPresent()) return notFound("Shop hasnt Stock");

         for (Product product : combo.getProducts().keySet()) {
             int value = combo.getProducts().get(product);
             stockController.sell(shopOptional.get().getId(), product.getId(), value);
         }

         return ok(Json.toJson(shopOptional.get()));
     }


}
