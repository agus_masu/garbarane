package models.products;

import models.BaseModel;

import java.util.concurrent.atomic.AtomicInteger;

public class Category extends BaseModel {
    private String name;
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);


    public Category(String name) {
        super(ID_GENERATOR.getAndIncrement());
        this.name = name;
    }
}
