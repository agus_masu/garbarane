package models.products;

import java.util.List;

public class StockeableProduct extends Product {

    private int height, weight, tall, anchor;

    public StockeableProduct(String name, String description, float actualPrice, List<Category> categories, int height, int weight, int tall, int anchor) {
        super(name, description, actualPrice, categories);
        this.height = height;
        this.weight = weight;
        this.tall = tall;
        this.anchor = anchor;
    }



    public StockeableProduct(){

    }

    @Override
    public ProductType getProductType() {
        return ProductType.STOCKEABLE;
    }
}
