package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import models.Interfaces.Sellable;

import java.util.concurrent.atomic.AtomicInteger;

public class User extends BaseModel {

    private Cart cart;
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
    private String name, surname, email;

    public User(){
        super(ID_GENERATOR.getAndIncrement());
        cart = new Cart();
    }

    public User(int id, String name, String surname, String email) {
        super(id);
        this.name = name;
        this.surname = surname;
        this.email = email;
        cart = new Cart();
    }

    public void addSellablesToCart(Sellable sellable, int quantity){
        cart.addSellable(sellable, quantity);
    }

    @JsonIgnore
    public Cart getCart(){return cart;}

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }
}
