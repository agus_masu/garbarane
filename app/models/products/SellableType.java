package models.products;

public enum SellableType {
    COMBO, SINGLE
}
