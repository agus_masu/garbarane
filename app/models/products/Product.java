package models.products;

import models.BaseModel;
import models.Interfaces.Sellable;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class Product extends BaseModel implements Sellable {
    private String name, description;
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
    private float price;
    private List<Category> categories;


    public Product(String name, String description, float actualPrice, List<Category> categories) {
        super(ID_GENERATOR.getAndIncrement());
        this.name = name;
        this.description = description;
        this.price = actualPrice;
        this.categories = categories;
    }

    public Product(){
        super(ID_GENERATOR.getAndIncrement());
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public static AtomicInteger getIdGenerator() {
        return ID_GENERATOR;
    }

    public float getPrice() {
        return price;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public SellableType getType(){return SellableType.SINGLE;}

    public abstract ProductType getProductType();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return Float.compare(product.price, price) == 0 &&
                Objects.equals(name, product.name) &&
                Objects.equals(description, product.description) &&
                Objects.equals(categories, product.categories);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, description, price, categories);
    }

}

