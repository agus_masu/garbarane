package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import models.products.Product;
import models.products.ProductType;
import models.products.StockeableProduct;
import play.api.Mode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Shop extends BaseModel{
    private String name;
    private static AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
    private Map<StockeableProduct, Integer> stockeableStock;


    public Shop(String name){
        super(ID_GENERATOR.getAndIncrement());
        stockeableStock = new HashMap<>();
    }

    public Shop(){
        super(ID_GENERATOR.getAndIncrement());
        stockeableStock = new HashMap<>();
    }

    public int getStock(Product product){
        if (!stockeableStock.containsKey(product)) return 0;
        return stockeableStock.get(product);
    }

    public String getName() {
        return name;
    }

    public void addToStock(StockeableProduct product, int quantity){
        if (stockeableStock.containsKey(product)) {
            final int value = stockeableStock.get(product) + quantity;
            stockeableStock.put(product, value);
        }
        else stockeableStock.put(product, quantity);
    }

    public boolean sell(Product product, int quantity){
        if (product.getProductType() == ProductType.VIRTUAL) return false;
        StockeableProduct stockable = ((StockeableProduct) product);
        if (!stockeableStock.containsKey(stockable)) return false;
        int value = stockeableStock.get(stockable) - quantity;
        stockeableStock.put(stockable, value);
        return true;
    }

    public boolean sell(Product product){return sell(product,1);}

    @JsonIgnore
    public Map<StockeableProduct, Integer> getStockeableStock() {
        return stockeableStock;
    }
}
