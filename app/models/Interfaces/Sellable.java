package models.Interfaces;

import models.products.Category;
import models.products.SellableType;

import java.util.List;

public interface Sellable {

    float getPrice();

    SellableType getType();

    String getName();

    List<Category> getCategories();
}
